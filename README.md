# golearn

## Go语言
    1、静态类型开发语言
    2、自动垃圾回收
    3、丰富的内置类型
    4、函数多返回值
    5、错误处理
    6、匿名函数
    7、并发编程
    8、反射
    9、defer
    
    非常适合服务器编排、网络编程（Web应用、API应用）和分布式编程等
    
## 安装
    https://golang.org/dl/
    
### 源码编译安装

### Window安装
    GOPAHTH=D:\goproject 工作目录
    GOROOT=D:\Go\ 安装目录
    
    
    win + r  go
    go version
### 开发工具